CREATE TABLE CFOPOrigemDestino (
	origem		char(2)				not null,
	destino		char(2)				not null,
	Cfop		numeric(15,3)		not null,
	dtCadastro	datetime default current_timestamp null,

	constraint pk_CFOPOrigemDestino primary key clustered (origem, destino)
);

INSERT INTO CFOPOrigemDestino (origem, destino, Cfop, dtCadastro) VALUES ('SP', 'RJ', 6.000, CURRENT_TIMESTAMP);
INSERT INTO CFOPOrigemDestino (origem, destino, Cfop, dtCadastro) VALUES ('SP', 'PE', 6.001, CURRENT_TIMESTAMP);
INSERT INTO CFOPOrigemDestino (origem, destino, Cfop, dtCadastro) VALUES ('SP', 'MG', 6.002, CURRENT_TIMESTAMP);
INSERT INTO CFOPOrigemDestino (origem, destino, Cfop, dtCadastro) VALUES ('SP', 'PB', 6.003, CURRENT_TIMESTAMP);
INSERT INTO CFOPOrigemDestino (origem, destino, Cfop, dtCadastro) VALUES ('SP', 'PR', 6.004, CURRENT_TIMESTAMP);
INSERT INTO CFOPOrigemDestino (origem, destino, Cfop, dtCadastro) VALUES ('SP', 'PI', 6.005, CURRENT_TIMESTAMP);
INSERT INTO CFOPOrigemDestino (origem, destino, Cfop, dtCadastro) VALUES ('SP', 'RO', 6.006, CURRENT_TIMESTAMP);
INSERT INTO CFOPOrigemDestino (origem, destino, Cfop, dtCadastro) VALUES ('MG', 'TO', 6.008, CURRENT_TIMESTAMP);
INSERT INTO CFOPOrigemDestino (origem, destino, Cfop, dtCadastro) VALUES ('SP', 'SE', 6.009, CURRENT_TIMESTAMP);
INSERT INTO CFOPOrigemDestino (origem, destino, Cfop, dtCadastro) VALUES ('SP', 'PA', 6.010, CURRENT_TIMESTAMP);
 							
INSERT INTO CFOPOrigemDestino (origem, destino, Cfop, dtCadastro) VALUES ('MG', 'RJ', 6.000, CURRENT_TIMESTAMP);
INSERT INTO CFOPOrigemDestino (origem, destino, Cfop, dtCadastro) VALUES ('MG', 'PE', 6.001, CURRENT_TIMESTAMP);
INSERT INTO CFOPOrigemDestino (origem, destino, Cfop, dtCadastro) VALUES ('MG', 'MG', 6.002, CURRENT_TIMESTAMP);
INSERT INTO CFOPOrigemDestino (origem, destino, Cfop, dtCadastro) VALUES ('MG', 'PB', 6.003, CURRENT_TIMESTAMP);
INSERT INTO CFOPOrigemDestino (origem, destino, Cfop, dtCadastro) VALUES ('MG', 'PR', 6.004, CURRENT_TIMESTAMP);
INSERT INTO CFOPOrigemDestino (origem, destino, Cfop, dtCadastro) VALUES ('MG', 'PI', 6.005, CURRENT_TIMESTAMP);
INSERT INTO CFOPOrigemDestino (origem, destino, Cfop, dtCadastro) VALUES ('MG', 'RO', 6.006, CURRENT_TIMESTAMP);
INSERT INTO CFOPOrigemDestino (origem, destino, Cfop, dtCadastro) VALUES ('MG', 'TO', 6.008, CURRENT_TIMESTAMP);
INSERT INTO CFOPOrigemDestino (origem, destino, Cfop, dtCadastro) VALUES ('MG', 'SE', 6.009, CURRENT_TIMESTAMP);
INSERT INTO CFOPOrigemDestino (origem, destino, Cfop, dtCadastro) VALUES ('MG', 'PA', 6.010, CURRENT_TIMESTAMP);