USE [Teste]
GO

CREATE PROCEDURE [dbo].[P_SEL_SOMA_ITENS_POR_CFOP]

AS

SELECT  

  Cfop [CFOP],
  COALESCE(SUM(BaseIcms), 0)  [Valor Total da Base de ICMS],
  COALESCE(SUM(ValorIcms), 0) [Valor Total do ICMS],
  COALESCE(SUM(BaseIPI), 0)   [Valor Total da Base de IPI],
  COALESCE(SUM(BaseIPI), 0)   [Valor Total do IPI]

  FROM [Teste].[dbo].[NotaFiscalItem]
  GROUP BY Cfop;

