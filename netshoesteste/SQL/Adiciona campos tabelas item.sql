ALTER TABLE NotaFiscalItem
ADD BaseIPI DECIMAL NULL;

ALTER TABLE NotaFiscalItem
ADD AliquotaIPI DECIMAL NULL;

ALTER TABLE NotaFiscalItem
ADD ValorIPI DECIMAL NULL;

ALTER TABLE NotaFiscalItem 
ADD PercDesconto decimal null