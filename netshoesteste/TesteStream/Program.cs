﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Collections;

namespace TesteStream
{
    class Program
    {
        static void Main(string[] args)
        {
            var file = @"c:\XML\teste.txt";
            var dir = @"c:\XML";

            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }

            if (!File.Exists(file))
            {
                string[] li = { "l", "aAbBABacfe" };
                FileStream f = File.Create(file);
                f.Close();
                f.Dispose();

                File.WriteAllLines(file, li);
            }

            StreamReader read = new StreamReader(file);

            String consoantes = "bcdfghjklmnpqrstvxwyz";
            String vogais = "aeiou";

            int sPrimeiraConsoantePos = 0;
            int numeroRepeticaoVogal = 0;
            string pVogalNRepetir = ""; 

            ArrayList arr = new ArrayList();
            string l = String.Empty;
            while (l != null)
            {
                l = read.ReadLine();
                if (l != null)
                {
                    arr.Add(l);
                }
            }

            String stStream = String.Join("", arr.ToArray());

            for(int i = 0; i < stStream.Length; i++)
            {
                if (consoantes.IndexOf(stStream[i].ToString().ToLower()) > -1)
                {
                    sPrimeiraConsoantePos = consoantes.IndexOf(stStream[i].ToString().ToLower());
                    break;
                }
            }
            stStream = stStream.Substring(sPrimeiraConsoantePos);

            for (int i = 0; i < stStream.Length; i++)
            {
                if (vogais.IndexOf(stStream[i].ToString().ToLower()) > -1)
                {
                    numeroRepeticaoVogal = procurarVogal(stStream[i].ToString().ToLower(), stStream);
                    if(numeroRepeticaoVogal == 1)
                    {
                        pVogalNRepetir = stStream[i].ToString().ToLower();
                        break;
                    }
                }
            }

            if(numeroRepeticaoVogal > 1)
            {
                Console.WriteLine("Nenhum caractere com as condições necessárias foi encontrado!");
            }
            else
            {
                Console.WriteLine("Vogal: " + pVogalNRepetir + " Numero repeticoes:" + numeroRepeticaoVogal);
            }
            read.Close();
            Console.ReadLine();
            Console.ReadLine();
        }

        public static int procurarVogal(string vogal, string stStream)
        {
            int numeroRepeticoes = 0;
            for (int i = 0; i < stStream.Length; i++)
            {
                if (stStream[i].ToString().ToLower() == vogal)
                {
                    ++numeroRepeticoes;
                }
                    
            }
            return numeroRepeticoes;
        }
    }
}
