﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Imposto.Core.Domain;
using System.Configuration;
using System.Data.SqlClient;

namespace Imposto.Core.Data
{
    public class NotaFiscalRepository
    {
        private static string stConection { get; set; }
        private static SqlConnection connection = new SqlConnection();
        public NotaFiscalRepository()
        {
            stConection = ConfigurationManager.ConnectionStrings["connectionLocal"].ToString();
        }
        private SqlConnection AbrirConexao()
        {
            try
            {
                connection = new SqlConnection(stConection);

                if (connection.State != System.Data.ConnectionState.Open)
                {
                    connection.Open();
                }

                return connection;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        private void FecharConexao()
        {
            if(connection.State != System.Data.ConnectionState.Closed)
                connection.Close();
        }

        public NotaFiscal InserirNotaFiscal(NotaFiscal enNotaFiscao)
        {
            try
            {
                AbrirConexao();
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.CommandText = "P_NOTA_FISCAL";

                command.Parameters.Add(new SqlParameter("@pId", null));
                command.Parameters.Add(new SqlParameter("@pNumeroNotaFiscal", enNotaFiscao.NumeroNotaFiscal));
                command.Parameters.Add(new SqlParameter("@pSerie", enNotaFiscao.Serie));
                command.Parameters.Add(new SqlParameter("@pNomeCliente", enNotaFiscao.NomeCliente));
                command.Parameters.Add(new SqlParameter("@pEstadoDestino", enNotaFiscao.EstadoDestino));
                command.Parameters.Add(new SqlParameter("@pEstadoOrigem", enNotaFiscao.EstadoOrigem));
                var idNota = command.ExecuteScalar();
                enNotaFiscao.Id = Convert.ToInt32(idNota);

                command.Dispose();
                enNotaFiscao = InserirNotaFiscalItem(enNotaFiscao);
                FecharConexao();

                return enNotaFiscao;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        private NotaFiscal InserirNotaFiscalItem(NotaFiscal enNotaFiscao)
        {
            SqlCommand command = null;

            foreach (NotaFiscalItem item in enNotaFiscao.ItensDaNotaFiscal)
            {
                command = new SqlCommand();
                command.Connection = connection;
                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.CommandText = "P_NOTA_FISCAL_ITEM";

                command.Parameters.Add(new SqlParameter("@pId", null));
                command.Parameters.Add(new SqlParameter("@pIdNotaFiscal", enNotaFiscao.Id));
                command.Parameters.Add(new SqlParameter("@pCfop", item.Cfop));
                command.Parameters.Add(new SqlParameter("@pTipoIcms", item.TipoIcms));
                command.Parameters.Add(new SqlParameter("@pBaseIcms", item.BaseIcms));
                command.Parameters.Add(new SqlParameter("@pAliquotaIcms", item.AliquotaIcms));
                command.Parameters.Add(new SqlParameter("@pValorIcms", item.ValorIcms));
                command.Parameters.Add(new SqlParameter("@pNomeProduto", item.NomeProduto));
                command.Parameters.Add(new SqlParameter("@pCodigoProduto", item.CodigoProduto));
                command.Parameters.Add(new SqlParameter("@AliquotaIPI", item.AliquotaIPI));
                command.Parameters.Add(new SqlParameter("@ValorIPI", item.AliquotaIPI));
                command.Parameters.Add(new SqlParameter("@BaseIPI", item.BaseIPI));
                command.Parameters.Add(new SqlParameter("@PercDesconto", item.PercDesconto));
                command.ExecuteNonQuery();
                command.Dispose();
            }

            return enNotaFiscao;
        }
    }
}
