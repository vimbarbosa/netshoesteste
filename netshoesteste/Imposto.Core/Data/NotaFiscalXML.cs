﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Imposto.Core.Domain;
using System.Xml;
using System.IO;
using System.Configuration;

namespace Imposto.Core.Data
{
    public class NotaFiscalXML
    {
        private static string stCaminho = ConfigurationManager.AppSettings["CaminhoArquivoXML"].ToString();
        private static string stNomeArquivo { get; set; }
        /// <summary>
        /// Metodo para geração do arquivo xml
        /// </summary>
        /// <param name="enNotaFiscal"></param>
        /// <returns></returns>
        public NotaFiscal GerarXML(NotaFiscal enNotaFiscal)
        {
            try
            {
                if (!Directory.Exists(stCaminho))
                {
                    Directory.CreateDirectory(stCaminho);
                }
                stNomeArquivo = String.Concat(System.IO.Path.GetRandomFileName(), ".XML");

                XmlTextWriter writer = new XmlTextWriter(Path.Combine(stCaminho, stNomeArquivo), null);

                writer.WriteStartDocument();
                writer.WriteStartElement("NotaFiscal");
                writer.WriteElementString("NumeroNotaFiscal", enNotaFiscal.NumeroNotaFiscal.ToString());
                writer.WriteElementString("Serie", enNotaFiscal.Serie.ToString());
                writer.WriteElementString("NomeCliente", enNotaFiscal.NomeCliente.ToString());
                writer.WriteElementString("EstadoDestino", enNotaFiscal.EstadoDestino);
                writer.WriteElementString("EstadoOrigem", enNotaFiscal.EstadoOrigem);

                writer.WriteStartElement("ItensDaNotaFiscal");

                foreach (NotaFiscalItem item in enNotaFiscal.ItensDaNotaFiscal)
                {
                    writer.WriteStartElement("NotaFiscalItem");

                    writer.WriteElementString("Cfop", item.Cfop.ToString());
                    writer.WriteElementString("TipoIcms", item.TipoIcms);
                    writer.WriteElementString("BaseIcms", item.BaseIPI.ToString());
                    writer.WriteElementString("BaseIPI", item.BaseIPI.ToString());
                    writer.WriteElementString("ValorIPI", item.ValorIPI.ToString());
                    writer.WriteElementString("AliquotaIPI", item.AliquotaIPI.ToString());
                    writer.WriteElementString("AliquotaIcms", item.AliquotaIcms.ToString());
                    writer.WriteElementString("ValorIcms", item.ValorIcms.ToString());
                    writer.WriteElementString("NomeProduto", item.NomeProduto.ToString());
                    writer.WriteElementString("CodigoProduto", item.CodigoProduto.ToString());

                    writer.WriteEndElement();
                }

                writer.WriteEndElement();
                writer.WriteEndElement();
                writer.Close();

                if (VerificarXMLGerado())
                    return enNotaFiscal;
                else
                {
                    throw new Exception("Arquivo xml não pode ser criado, verifique o arquivo de configuração e ou as permissões necessárias");
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// Metodo para verificar se arquivo xml foi gerado
        /// </summary>
        /// <returns></returns>
        private bool VerificarXMLGerado()
        {
            bool ret = false;
            if (Directory.Exists(stCaminho) && File.Exists(Path.Combine(stCaminho, stNomeArquivo)))
                ret = true;

            return ret;
        }


    }
}
