﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Imposto.Core.Domain;
using System.Configuration;
using System.Data.SqlClient;

namespace Imposto.Core.Data
{
    class EstadosRepository
    {
        private static string stConection { get; set; }
        private static SqlConnection connection = new SqlConnection();
        public EstadosRepository()
        {
            stConection = ConfigurationManager.ConnectionStrings["connectionLocal"].ToString();
        }
        private SqlConnection AbrirConexao()
        {
            try
            {
                connection = new SqlConnection(stConection);

                if (connection.State != System.Data.ConnectionState.Open)
                {
                    connection.Open();
                }

                return connection;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<EstadosCFOP> ConsultarCFOPEstados()
        {
            try
            {
                List<EstadosCFOP> lis = new List<EstadosCFOP>();
                AbrirConexao();
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.CommandText = "P_SEL_ORIGEM_DESTINO_CFOP";
                SqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        EstadosCFOP enEstadosCFOP = new EstadosCFOP();
                        enEstadosCFOP.EstadoOrigem = reader["origem"].ToString();
                        enEstadosCFOP.EstadoDestino = reader["destino"].ToString();
                        enEstadosCFOP.CFOP = Convert.ToDecimal(reader["CFOP"]);
                        lis.Add(enEstadosCFOP);
                    }
                }
                command.Dispose();
                FecharConexao();

                return lis;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void FecharConexao()
        {
            if (connection.State != System.Data.ConnectionState.Closed)
                connection.Close();
        }
    }
}
