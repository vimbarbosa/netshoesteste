﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Threading.Tasks;
using Imposto.Core.Data;
using AutoMapper;
using System.Configuration;

namespace Imposto.Core.Domain
{
    public class NotaFiscal
    {
        public int Id { get; set; }
        public int NumeroNotaFiscal { get; set; }
        public int Serie { get; set; }
        public string NomeCliente { get; set; }
        public string EstadoDestino { get; set; }
        public string EstadoOrigem { get; set; }
        public List<EstadosCFOP> lisEstadosCFOP { get; set; }
        public List<NotaFiscalItem> ItensDaNotaFiscal { get; set; }

        public NotaFiscal()
        {
            ItensDaNotaFiscal = new List<NotaFiscalItem>();
        }

        public NotaFiscal EmitirNotaFiscal(Pedido pedido)
        {
            this.NumeroNotaFiscal = 99999;
            this.EstadoOrigem = pedido.EstadoOrigem;
            this.EstadoDestino = pedido.EstadoDestino;

            //escolhe cfop baseado nos estados selecionados de origem e destino
            string cfop = BuscarValorCFOP();
            
            //gerar numero de serie da nota fiscal
            this.Serie = GerarNumeroDeSerieDaNotaFiscal();
            
            //clona os dados da classe pedido para a classe nota fiscal quando os campos coincidirem
            NotaFiscal enNotaFiscal = CopiarDadosDoPedidoParaNotaFiscal(pedido);

            foreach (PedidoItem itemPedido in pedido.ItensDoPedido)
            {
                NotaFiscalItem notaFiscalItem = new NotaFiscalItem();

                //verifica se o desconto se aplica ao estado destino
                notaFiscalItem.PercDesconto = BuscarPercentualDescontoDoItemPedido();
                notaFiscalItem.Cfop = cfop;
                notaFiscalItem.BaseIPI = itemPedido.ValorItemPedido;

                //aplica-se regra para cada campo
                notaFiscalItem.BaseIcms         = AplicaBaseICMS(itemPedido.ValorItemPedido, cfop);
                notaFiscalItem.AliquotaIPI      = AplicaAliquotaIPI(itemPedido.Brinde);
                notaFiscalItem.AliquotaIcms     = AplicaAliquotaICMS(itemPedido.Brinde);
                notaFiscalItem.TipoIcms         = AplicaTipoICMS(itemPedido.Brinde);
                notaFiscalItem.ValorIcms        = AplicaValorICMS(notaFiscalItem.BaseIcms, notaFiscalItem.AliquotaIcms);
                notaFiscalItem.ValorIPI         = AplicaValorIPI(notaFiscalItem.BaseIPI, notaFiscalItem.AliquotaIPI); 
                notaFiscalItem.NomeProduto      = itemPedido.NomeProduto;
                notaFiscalItem.CodigoProduto    = itemPedido.CodigoProduto;

                enNotaFiscal.ItensDaNotaFiscal.Add(notaFiscalItem);
            }

            //gerar o arquivo XML 
            GerarXML(enNotaFiscal);

            //após verificar se o arquivo xml existe, persiste os dados no banco de dados
            InserirNotaFiscalDB(enNotaFiscal);

            return enNotaFiscal;
        }

        /// <summary>
        /// Cria arquivo xml e verifica se foi criado
        /// </summary>
        /// <param name="enNotaFiscal"></param>
        /// <returns></returns>
        private NotaFiscal GerarXML(NotaFiscal enNotaFiscal)
        {
            NotaFiscalXML xml = new NotaFiscalXML();
            enNotaFiscal = xml.GerarXML(enNotaFiscal);

            return enNotaFiscal;
        }

        /// <summary>
        /// Persiste os dados da nota no banco de dados
        /// </summary>
        /// <param name="enNotaFiscal"></param>
        /// <returns></returns>
        private NotaFiscal InserirNotaFiscalDB(NotaFiscal enNotaFiscal)
        {
            NotaFiscalRepository rep = new NotaFiscalRepository();
            rep.InserirNotaFiscal(enNotaFiscal);

            return enNotaFiscal;                  
        }

        /// <summary>
        /// //Copiar os dados de pedido para nota fiscal quando coincidirem.
        /// </summary>
        /// <param name="pedido"></param>
        /// <returns></returns>
        private NotaFiscal CopiarDadosDoPedidoParaNotaFiscal(Pedido pedido)
        {
            NotaFiscal enNotaFiscal = new NotaFiscal();
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<Pedido, NotaFiscal>();
            });
            IMapper mapper = config.CreateMapper();
            enNotaFiscal = mapper.Map<Pedido, NotaFiscal>(pedido);
            enNotaFiscal.NumeroNotaFiscal = this.NumeroNotaFiscal;
            enNotaFiscal.Serie = this.Serie;

            return enNotaFiscal;
        }

        /// <summary>
        /// Gera novo número de série para nota fiscal
        /// </summary>
        /// <returns>numero de serie</returns>
        private int GerarNumeroDeSerieDaNotaFiscal()
        {
            int serie = 0;
            serie = new Random().Next(Int32.MaxValue);
            return serie;
        }

        /// <summary>
        /// verifica se o destino está na região sudeste para definir percetual de desconto
        /// </summary>
        /// <param name="stEstadoDestino">Estado destino</param>
        /// <returns>retorna o valor do percentual desconto</returns>
        public double BuscarPercentualDescontoDoItemPedido()
        {
            double dbPercDesconto = 0;

            var values = Enum.GetValues(typeof(EstadosCFOP.EstadosDescontos));

            for (int i = 0; i < values.Length; i++)
            {
                if (values.GetValue(i).ToString().Equals(this.EstadoDestino))
                {
                    dbPercDesconto = 0.10;
                }
            }

            return dbPercDesconto;
        }

        /// <summary>
        /// Busca o valor da CFOP por estados origem e destino
        /// </summary>
        /// <param name="lisEstadosCFOP"></param>
        /// <returns></returns>
        public string BuscarValorCFOP()
        {
            //gera lista de estados origem destino e seus respectivos valores de cfop
            this.lisEstadosCFOP = EstadosCFOP.ConsultarCFOPEstados();
            string CFOP = String.Empty;
            CFOP =  (from e in this.lisEstadosCFOP
                                   where e.EstadoOrigem == this.EstadoOrigem && e.EstadoDestino == this.EstadoDestino
                                   select e.CFOP).FirstOrDefault().ToString();
            return CFOP;
        }

        public double AplicaAliquotaIPI(bool Brinde)
        {
            double AliquotaIPI = !Brinde ? 0 : 0.10;
            return AliquotaIPI;
        }

        public double AplicaAliquotaICMS(bool Brinde)
        {
            double AliquotaICMS = 0.17;
            if (this.EstadoOrigem == this.EstadoDestino || Brinde)
            {
                AliquotaICMS = 0.18;
            }
            return AliquotaICMS;
        }

        public string AplicaTipoICMS(bool Brinde)
        {
            string tipoICMS = "10";
            if(this.EstadoOrigem == this.EstadoDestino || Brinde)
            {
                tipoICMS = "60";
            }
            return tipoICMS;
        }

        public double AplicaValorICMS(double BaseIcms, double AliquotaIcms)
        {
            double ValorIcms = BaseIcms * AliquotaIcms; ;
            return ValorIcms;
        }
        public double AplicaValorIPI(double BaseIPI, double AliquotaIPI)
        {
            double ValorIPI = BaseIPI * AliquotaIPI;
            return ValorIPI;
        }

        public double AplicaBaseICMS(double ValorItemPedido, string Cfop)
        {
            double BaseIcms = 0;
            if (Cfop == "6.009")
                BaseIcms = ValorItemPedido * 0.90; //redução de base
            else
                BaseIcms = ValorItemPedido;
            return BaseIcms;
        }
    }
}
