﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Imposto.Core.Data;

namespace Imposto.Core.Domain
{
    public class EstadosCFOP
    {
        public string EstadoOrigem { get; set; }

        public string EstadoDestino { get; set; }

        public bool EstadoDesconto { get; set; }

        public decimal CFOP { get; set; }
        
        /// <summary>
        /// enum com os estados que devem receber descontos, no caso da regra atual apenas da região sudeste.
        /// </summary>
        public enum EstadosDescontos
        {
            MG, RJ, SP                
        }
        /// <summary>
        /// Consulta lista de CFOPS por estados origem e destino
        /// </summary>
        /// <returns></returns>
        public static List<EstadosCFOP> ConsultarCFOPEstados()
        {
            EstadosRepository rep = new EstadosRepository();
            List<EstadosCFOP> enEstadosCFOP = rep.ConsultarCFOPEstados();
            return enEstadosCFOP;
        }
        /// <summary>
        /// Verificar validade de (se existe a possibilidade do envio do pedido) estados origem e destino que foram informados
        /// </summary>
        /// <param name="EstadoOrigem"></param>
        /// <param name="EstadoDestino"></param>
        /// <returns></returns>
        public static bool VerificarEstadosOrigemDestino(String EstadoOrigem, String EstadoDestino)
        {
            bool ret = false;
            List<EstadosCFOP> lisEstadosCFOP = ConsultarCFOPEstados();

            var existe = (from e in lisEstadosCFOP
                                   where e.EstadoOrigem == EstadoOrigem && e.EstadoDestino == EstadoDestino
                            select e.CFOP).FirstOrDefault();
            ret = (existe > 0);
            return ret;
        }
        /// <summary>
        /// Buscar estados origem 
        /// </summary>
        /// <returns></returns>
        public static List<String> ConsultarEstadosOrigem()
        {
            List<EstadosCFOP> lis = ConsultarCFOPEstados();
            var source = (from l in lis select l.EstadoOrigem).Distinct().ToList();
            return source;
        }
        /// <summary>
        /// Buscar quais estados destino podem ser associados ao estado origem
        /// </summary>
        /// <param name="EstadoOrigem"></param>
        /// <returns></returns>
        public static List<String> BuscarEstadosDestinoPorOrigem(String EstadoOrigem)
        {
            List<EstadosCFOP> lis = ConsultarCFOPEstados();
            var source = (from l in lis where l.EstadoOrigem == EstadoOrigem select l.EstadoDestino).Distinct().ToList();
            return source;
        }
    }
}
