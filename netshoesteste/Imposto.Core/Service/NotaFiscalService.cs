﻿using Imposto.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Imposto.Core.Service
{
    public class NotaFiscalService : IDisposable        
    {
        public NotaFiscalService()
        {
            this.pedidoItem = new PedidoItem();
            this.pedido = new Pedido();
        }
        public Pedido pedido { get; set; }
        public PedidoItem pedidoItem { get; set; }
        public NotaFiscal GerarNotaFiscal()
        {
            NotaFiscal notaFiscal = new NotaFiscal();
            notaFiscal = notaFiscal.EmitirNotaFiscal(this.pedido);

            return notaFiscal;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~NotaFiscalService()
        {
            Dispose(false);
        }

        protected virtual void Dispose(bool disposing)
        {

        }
    }
}
