﻿using Imposto.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Imposto.Core.Service
{
    public class EstadosService : IDisposable
    {
        /// <summary>
        /// Verifica validade de estados origem e destino
        /// </summary>
        /// <param name="EstadoOrigem"></param>
        /// <param name="EstadoDestino"></param>
        /// <returns></returns>
        public bool VerificarEstadosOrigemDestino(String EstadoOrigem, String EstadoDestino)
        {
            bool ret;
            ret = EstadosCFOP.VerificarEstadosOrigemDestino(EstadoOrigem, EstadoDestino);
            return ret;
        }

        public List<String> BuscarEstadosOrigem()
        {
            List<String> lis = new List<string>();
            lis = EstadosCFOP.ConsultarEstadosOrigem();
            return lis;
        }
        public List<String> BuscarEstadosDestinoPorOrigem(String EstadoOrigem)
        {
            List<String> lis = new List<string>();
            lis = EstadosCFOP.BuscarEstadosDestinoPorOrigem(EstadoOrigem);
            return lis;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
         
        ~EstadosService()
        {
            Dispose(false);
        }
        
        protected virtual void Dispose(bool disposing)
        {

        }
    }
}
