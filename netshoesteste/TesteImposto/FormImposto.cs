﻿using Imposto.Core.Service;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TesteImposto
{
    public partial class FormImposto : Form
    {
        public FormImposto()
        {
            InitializeComponent();
            PreparaFormulario();
        }
        private void PreparaFormulario()
        {
            dataGridViewPedidos.AutoGenerateColumns = true;
            dataGridViewPedidos.DataSource = GetTablePedidos();
            ResizeColumns();
            populateEstadosOrigem();
        }
        private void LimparFormulario()
        {
            comboBoxOrigem.SelectedIndex = -1;
            comboBoxDestino.SelectedIndex = -1;
            textBoxNomeCliente.Text = String.Empty;
            dataGridViewPedidos.DataSource = null;
            comboBoxDestino.Items.Clear();
            comboBoxOrigem.Items.Clear();
        }
        private void populateEstadosOrigem()
        {
            List<String> lis = null;
            EstadosService service = new EstadosService();
            lis = service.BuscarEstadosOrigem();

            foreach(string item in lis)
            {
                comboBoxOrigem.Items.Add(item);
            }
        }
        private void populateEstadosDestino()
        {
            if(comboBoxOrigem.SelectedItem != null)
            {
                List<String> lis = null;
                EstadosService service = new EstadosService();
                lis = service.BuscarEstadosDestinoPorOrigem(comboBoxOrigem.SelectedItem.ToString());

                foreach (string item in lis)
                {
                    comboBoxDestino.Items.Add(item);
                }
                comboBoxDestino.Visible = true;
            }
        }
        private void ResizeColumns()
        {
            double mediaWidth = dataGridViewPedidos.Width / dataGridViewPedidos.Columns.GetColumnCount(DataGridViewElementStates.Visible);

            for (int i = dataGridViewPedidos.Columns.Count - 1; i >= 0; i--)
            {
                var coluna = dataGridViewPedidos.Columns[i];
                coluna.Width = Convert.ToInt32(mediaWidth);
            }   
        }

        private object GetTablePedidos()
        {
            DataTable table = new DataTable("pedidos");
            table.Columns.Add(new DataColumn("Nome do produto", typeof(string)));
            table.Columns.Add(new DataColumn("Codigo do produto", typeof(string)));
            table.Columns.Add(new DataColumn("Valor", typeof(decimal)));
            table.Columns.Add(new DataColumn("Brinde", typeof(bool)));
                     
            return table;
        }

        private void buttonGerarNotaFiscal_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(textBoxNomeCliente.Text))
            {
                MessageBox.Show("O nome do cliente deve ser informado");
                return;
            }
            //chama serviço de consulta de regras de cfop por estados para validar estados
            using (EstadosService service = new EstadosService())
            {
                if (!service.VerificarEstadosOrigemDestino(comboBoxOrigem.Text, comboBoxDestino.Text))
                {
                    MessageBox.Show("Estado origem ou destino Inválido(os)");
                    return;
                }
            }
            DataTable table = (DataTable)dataGridViewPedidos.DataSource;
            if (table.Rows.Count == 0)
            {
                MessageBox.Show("Ao menos um ítem deve ser informado");
                return;
            }
            //Chama serviço de geração de nota fiscal
            using (NotaFiscalService service = new NotaFiscalService())
            {
                service.pedido.EstadoOrigem = comboBoxOrigem.Text;
                service.pedido.EstadoDestino = comboBoxDestino.Text;
                service.pedido.NomeCliente = textBoxNomeCliente.Text;

                foreach (DataRow row in table.Rows)
                {
                    service.pedidoItem.Brinde = Convert.ToBoolean(row["Brinde"]);
                    service.pedidoItem.CodigoProduto = row["Codigo do produto"].ToString();
                    service.pedidoItem.NomeProduto = row["Nome do produto"].ToString();
                    service.pedidoItem.ValorItemPedido = Convert.ToDouble(row["Valor"].ToString());
                    service.pedido.ItensDoPedido.Add(service.pedidoItem);
                }
                service.GerarNotaFiscal();
            }

            MessageBox.Show("Operação efetuada com sucesso");
            LimparFormulario();
            PreparaFormulario();
        }

        private void FormImposto_Load(object sender, EventArgs e)
        {

        }
        private void comboBoxOrigem_SelectedIndexChanged(object sender, EventArgs e)
        {
            populateEstadosDestino();
        }
    }
}
