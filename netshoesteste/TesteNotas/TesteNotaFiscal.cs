﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Imposto.Core.Service;
using Imposto.Core.Domain;

namespace TesteNotas
{
    [TestClass]
    public class TesteNotaFiscal
    {
        [TestMethod]
        public void Retornar_Valor_CFOP_Por_Estados()
        {
            string CFOPEsperado = "6,006";
            string CFOPRetornado = "";
            NotaFiscal enNotaFiscal = new NotaFiscal();

            enNotaFiscal.EstadoOrigem = "MG";
            enNotaFiscal.EstadoDestino = "RO";

            CFOPRetornado = enNotaFiscal.BuscarValorCFOP();
            Assert.AreEqual(CFOPEsperado, CFOPRetornado);
        }

        [TestMethod]
        public void Retornar_Desconto_Para_Estados_Sudeste()
        {
            NotaFiscal enNotaFiscal = new NotaFiscal();
            enNotaFiscal.EstadoOrigem = "MG";
            enNotaFiscal.EstadoDestino = "MG";
            double percDescontoEsperado = 0.10;
            double percDescontoRetornado = 0;

            percDescontoRetornado = enNotaFiscal.BuscarPercentualDescontoDoItemPedido();
            Assert.AreEqual(percDescontoEsperado, percDescontoRetornado);
        }

        [TestMethod]
        public void Retornar_Base_ICMS()
        {
            NotaFiscal enNotaFiscal = new NotaFiscal();
            enNotaFiscal.EstadoOrigem = "MG";
            enNotaFiscal.EstadoDestino = "MG";

            double BaseICMSEsperado = 450;
            double BaseICMSRetornado = 0;

            BaseICMSRetornado = enNotaFiscal.AplicaBaseICMS(500, "6.009");
            Assert.AreEqual(BaseICMSEsperado, BaseICMSRetornado);
        }

        [TestMethod]
        public void Retornar_Aliquota_IPI()
        {
            NotaFiscal enNotaFiscal = new NotaFiscal();
            enNotaFiscal.EstadoOrigem = "MG";
            enNotaFiscal.EstadoDestino = "MG";

            double AliquotaIPIEsperado = 0.10;
            double AliquotaIPIRetornado = 0;
            bool Brinde = true;

            AliquotaIPIRetornado = enNotaFiscal.AplicaAliquotaIPI(Brinde);
            Assert.AreEqual(AliquotaIPIEsperado, AliquotaIPIRetornado);
        }

        [TestMethod]
        public void Retornar_Aliquota_ICMS()
        {
            NotaFiscal enNotaFiscal = new NotaFiscal();
            enNotaFiscal.EstadoOrigem = "MG";
            enNotaFiscal.EstadoDestino = "MG";

            double AliquotaICMSEsperado = 0.18;
            double AliquotaICMSRetornado = 0;
            bool Brinde = false;

            AliquotaICMSRetornado = enNotaFiscal.AplicaAliquotaICMS(Brinde);
            Assert.AreEqual(AliquotaICMSEsperado, AliquotaICMSRetornado);
        }

        [TestMethod]
        public void Retornar_Tipo_ICMS()
        {
            NotaFiscal enNotaFiscal = new NotaFiscal();
            enNotaFiscal.EstadoOrigem = "MG";
            enNotaFiscal.EstadoDestino = "MG";

            string TipoICMSEsperado = "60";
            string TipoICMSRetornado = "";
            bool Brinde = false;

            TipoICMSRetornado = enNotaFiscal.AplicaTipoICMS(Brinde);
            Assert.AreEqual(TipoICMSEsperado, TipoICMSRetornado);
        }

        [TestMethod]
        public void Retornar_Valor_ICMS()
        {
            NotaFiscal enNotaFiscal = new NotaFiscal();
            enNotaFiscal.EstadoOrigem = "MG";
            enNotaFiscal.EstadoDestino = "MG";

            double ValorICMSEsperado = 100;
            double ValorICMSRetornado = 0;
            double BaseIcms = 10;
            double AliquotaIcms = 10;

            ValorICMSRetornado = enNotaFiscal.AplicaValorICMS(BaseIcms, AliquotaIcms);
            Assert.AreEqual(ValorICMSEsperado, ValorICMSRetornado);
        }
        [TestMethod]
        public void Retornar_Valor_IPI()
        {
            NotaFiscal enNotaFiscal = new NotaFiscal();
            enNotaFiscal.EstadoOrigem = "MG";
            enNotaFiscal.EstadoDestino = "MG";

            double ValorIPIEsperado = 100;
            double ValorIPIRetornado = 0;
            double BaseIPI = 10;
            double AliquotaIPI = 10;

            ValorIPIRetornado = enNotaFiscal.AplicaValorICMS(BaseIPI, AliquotaIPI);
            Assert.AreEqual(ValorIPIEsperado, ValorIPIRetornado);
        }

        [TestMethod]
        public void Retornar_Nota_Fiscal_Inserida()
        {
            NotaFiscal enNotaFiscal = null;
            using (NotaFiscalService service = new NotaFiscalService())
            {
                service.pedido.EstadoOrigem = "MG";
                service.pedido.EstadoDestino = "MG";
                service.pedido.NomeCliente = "Teste";
                service.pedidoItem.Brinde = true;
                service.pedidoItem.CodigoProduto = "123";
                service.pedidoItem.NomeProduto = "Produto teste";
                service.pedidoItem.ValorItemPedido = 100000;
                service.pedido.ItensDoPedido.Add(service.pedidoItem);
                enNotaFiscal = service.GerarNotaFiscal();
            }
            Assert.IsNotNull(enNotaFiscal);
        }
    }
}
